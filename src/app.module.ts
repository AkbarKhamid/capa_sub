import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TerminusModule } from '@nestjs/terminus';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { HealthController } from './health/health.controller';
import { StoresModule } from './stores/stores.module';

@Module({
  controllers: [AppController, HealthController],
  providers: [AppService],
  imports: [
    HttpModule,
    ConfigModule.forRoot({ isGlobal: true }),
    TerminusModule,
    // local
    StoresModule,
  ],
})
export class AppModule {}
