import { lastValueFrom } from 'rxjs';

import { Controller, Get, Param, Query } from '@nestjs/common';

import { PaginatedDto } from './dto/PaginatedDto';
import { Response } from './dto/Response';
import { StoreDetail } from './dto/StoreDetail';
import { Store } from './entities/store.entity';
import { StoresService } from './stores.service';

@Controller('stores')
export class StoresController {
  constructor(private readonly storesService: StoresService) {}
  @Get()
  async findAll(
    @Query('page') page: number,
  ): Promise<Response<PaginatedDto<StoreDetail>>> {
    const stores = this.storesService.findAll(page);
    const postcodes = stores.items.map((item: Store) => item.postcode);
    return await lastValueFrom(
      this.storesService.bulkPostcodeLookup(postcodes, 'postcodes'),
    )
      .then((res) => {
        stores.items.forEach((store) => {
          const index = res.result.findIndex(
            (item) => item.query === store.postcode,
          );
          stores.items[index] = {
            ...store,
            lng: index > -1 ? res.result[index].result.longitude : null,
            lat: index > -1 ? res.result[index].result.latitude : null,
          };
        });
        return {
          status: 200,
          result: stores,
        };
      })
      .catch((err) => {
        return {
          status: 200,
          result: stores,
          message: err.response?.data?.error,
        };
      });
  }

  @Get(':name')
  async findOne(@Param('name') name: string): Promise<Response<StoreDetail>> {
    const store = this.storesService.findOne(name);
    if (store === undefined) {
      return {
        status: 404,
        result: null,
        message: 'Store not found.',
      };
    }
    return await lastValueFrom(
      this.storesService.postcodeLookup(store.postcode, 'postcodes'),
    )
      .then((res) => {
        return {
          status: 200,
          result: {
            ...store,
            lng: res.result.longitude,
            lat: res.result.latitude,
          },
        };
      })
      .catch((err) => {
        return {
          status: 200,
          result: {
            ...store,
            lng: null,
            lat: null,
          },
          message: err.response?.data?.error,
        };
      });
  }

  @Get('/nearest/:name')
  async getNearest(
    @Param('name') name: string,
    @Query('radius') radius = 100,
    @Query('limit') limit = 10,
  ) {
    // lookup store
    const store = this.storesService.findOne(name);
    const retArr: StoreDetail[] = [];
    if (store === undefined) {
      return {
        status: 404,
        result: null,
        message: 'Store not found.',
      };
    }
    // get coords from postcodes
    const storeDetail = await lastValueFrom(
      this.storesService.postcodeLookup(store.postcode, 'postcodes'),
    )
      .then((res) => {
        return {
          status: 200,
          result: {
            ...store,
            lng: res.result.longitude,
            lat: res.result.latitude,
          },
        };
      })
      .catch((err) => {
        // Store does not have valid coords, skip
        return {
          status: 400,
          result: null,
          message: err.response?.data?.error,
        };
      });
    // no results, return the payload
    if (storeDetail.result === null) {
      return storeDetail;
    }
    // get nearest and return
    const { lng, lat } = storeDetail.result;
    return await lastValueFrom(
      this.storesService.nearestLookup(lng, lat, radius, limit),
    )
      .then((res) => {
        res.result.forEach((item) => {
          // json file does not have all the stores within the radius:
          // return the ones from the api call
          retArr.push({
            name: item.admin_district.replace(' ', '_'),
            postcode: item.postcode,
            lng: item.longitude,
            lat: item.latitude,
          });
        });
        return {
          status: 200,
          result: retArr.sort((a, b) => a.lng - b.lng), // sort longitude (north to south)
        };
      })
      .catch((err) => {
        return {
          status: 200,
          result: retArr,
          message: err.response?.data?.error,
        };
      });
  }
}
