import { HttpModule } from '@nestjs/axios';
import { Test, TestingModule } from '@nestjs/testing';

import { StoresController } from './stores.controller';
import { StoresService } from './stores.service';

describe('StoresController', () => {
  let controller: StoresController;
  let service: StoresService;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      controllers: [StoresController],
      providers: [StoresService],
    }).compile();
    service = module.get<StoresService>(StoresService);
    controller = module.get<StoresController>(StoresController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should return an array of store items', async () => {
    const serviceRes = {
      page: 1,
      pageCount: 10,
      items: [
        {
          name: 'Test',
          postcode: 'test',
          lng: -0.341337,
          lat: 51.741753,
        },
      ],
    };
    const controllerRes = {
      status: 200,
      result: serviceRes,
    };
    jest.spyOn(service, 'findAll').mockImplementation(() => serviceRes);
    const res = await controller.findAll(1);
    expect(res).toEqual(controllerRes);
  });

  it('should return one store item', async () => {
    const serviceRes = {
      name: 'Test',
      postcode: 'test',
      lng: null,
      lat: null,
    };
    const controllerRes = {
      status: 200,
      result: serviceRes,
    };
    jest.spyOn(service, 'findOne').mockImplementation(() => serviceRes);
    const res = await controller.findOne('test');
    expect(res).toEqual(controllerRes);
  });
});
