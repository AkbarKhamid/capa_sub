import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';

import * as STORES from '../../stores.json';
import { PostCodesDto } from './dto/PostCodes.dto';
import { PostCodesBulkResponse } from './dto/PostCodesBulkResponse';
import { Response } from './dto/Response';
import { StoreDetail } from './dto/StoreDetail';

@Injectable()
export class StoresService {
  constructor(private httpService: HttpService) {}

  bulkPostcodeLookup(
    postcodes: string[],
    endpoint: string,
  ): Observable<PostCodesBulkResponse> {
    return this.httpService
      .post(endpoint, { postcodes })
      .pipe(map((res) => res.data));
  }

  nearestLookup(
    lng: number,
    lat: number,
    radius: number,
    limit = 10,
    endpoint = 'postcodes',
  ): Observable<Response<PostCodesDto[]>> {
    let url = `${endpoint}?lon=${lng}&lat=${lat}&radius=${radius}&limit=${limit}`;
    if (radius > 10) {
      url = `${url}&widesearch=true`;
    }
    return this.httpService.get(url).pipe(map((res) => res.data));
  }

  postcodeLookup(
    postCode: string,
    endpoint: string,
  ): Observable<Response<PostCodesDto>> {
    return this.httpService
      .get(`${endpoint}/${postCode}`)
      .pipe(map((res) => res.data));
  }

  findAll(page = 1): {
    page: number;
    pageCount: number;
    items: StoreDetail[];
  } {
    const stores = STORES || [];
    // basic pagination
    const pageCount = Math.ceil(stores.length / 10);
    page = page > pageCount ? pageCount : page;
    const paginated = stores.slice(page * 10 - 10, page * 10);
    return {
      page: page,
      pageCount: pageCount,
      items: paginated,
    };
  }

  findOne(name: string): StoreDetail | undefined {
    const stores = STORES || [];
    return stores.find((item) => item.name === name);
  }
}
