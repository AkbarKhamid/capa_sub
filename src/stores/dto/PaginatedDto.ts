import { ApiProperty } from '@nestjs/swagger';

export class PaginatedDto<TData> {
  @ApiProperty()
  page: number;

  @ApiProperty()
  pageCount: number;
  @ApiProperty()
  items: TData[];
}
