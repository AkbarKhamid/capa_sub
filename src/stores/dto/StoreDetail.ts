import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class StoreDetail {
  @ApiProperty()
  name: string;
  @ApiProperty()
  postcode: string;
  @ApiPropertyOptional()
  lng?: number;
  @ApiPropertyOptional()
  lat?: number;
}
