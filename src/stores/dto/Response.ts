import { ApiProperty } from '@nestjs/swagger';

export class Response<TResult> {
  @ApiProperty()
  status: number;
  @ApiProperty({ isArray: true })
  result: TResult;
  @ApiProperty({ required: false })
  message?: string;
}
