import { PostCodesDto } from './PostCodes.dto';

export class PostCodesBulkResponse {
  status: number;
  result: Array<{
    query: string;
    result: PostCodesDto;
  }>;
}
