import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';

import { StoresController } from './stores.controller';
import { StoresService } from './stores.service';

@Module({
  imports: [HttpModule.register({ baseURL: 'https://api.postcodes.io/' })],
  controllers: [StoresController],
  providers: [StoresService],
})
export class StoresModule {}
