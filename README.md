## Description

### Capa coding task submission

### Uses [NestJS](https://github.com/nestjs/nest)

## Installation

```bash
$ yarn install
```

## Running the app

```bash
# development
$ yarn run start

# watch mode
$ yarn run start:dev

# production mode
$ yarn run start:prod
```

## Test

```bash
# unit tests
$ yarn run test

# e2e tests
$ yarn run test:e2e

# test coverage
$ yarn run test:cov
```

---

## API DOCS

- ```bash
  GET api/v1/stores/?page=${number}
  ```

  Response

  ```json
  {
    "status": 200,
    "result": {
      "page": "1",
      "pageCount": 10,
      "items": [
        {
          "name": "St_Albans",
          "postcode": "AL1 2RJ",
          "lng": -0.341337,
          "lat": 51.741753
        }

        ...
      ]
    }
  }
  ```

- ```bash
  GET api/v1/stores/${name:string}
  ```

  Response

  ```json
  {
    "status": 200,
    "result": {
      "name": "St_Albans",
      "postcode": "AL1 2RJ",
      "lng": -0.341337,
      "lat": 51.741753
    }
  }
  ```

- ```bash
  GET api/v1/stores/nearest/${name:string}?radius=${number}&limit=${number}
  ```

  Response

  ```json
  {
    "status": 200,
    "result": [
      {
        "name": "St_Albans",
        "postcode": "AL1 2RJ",
        "lng": -0.341337,
        "lat": 51.741753
      }

      ...
    ]
  }
  ```

---

## Answers

> If you had chosen to spend more time on this test, what would you have done differently?

- Better test coverage
- Better API docs with OpenAPI specs (tried doing it but didn't have enough time)

> What part did you find the hardest? What part are you most proud of? In both cases, why?

Hardest: Timing. Obviously, no project is small to have a room for improvements. I had a few more things that I tried add but unfortunately , did not manage it with my timing constraints.
Proud of: Codebase is `eslint`'ed, has task runners for tests (with jest), also follows enterprise level architecture, thanks to NestJS. It also has unit and E2E tests, although the coverage is very basic.

> What is one thing we could do to improve this test?

...
