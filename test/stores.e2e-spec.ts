import * as request from 'supertest';

import { HttpService } from '@nestjs/axios';
import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';

import { StoresModule } from '../src/stores/stores.module';
import { StoresService } from '../src/stores/stores.service';

describe('StoresController (e2e)', () => {
  let app: INestApplication;
  const httpService = new HttpService();
  const storesService = new StoresService(httpService);

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [StoresModule],
    })
      .overrideProvider(StoresService)
      .useValue(storesService)
      .compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/(GET) stores/St_Albans', async () => {
    const res: request.Response = await request(app.getHttpServer()).get(
      '/stores/St_Albans',
    );
    expect(res.status).toEqual(200);
    expect(res.type).toEqual('application/json');
    expect({
      name: res.body?.result.name,
      postcode: res.body?.result.postcode,
    }).toEqual(storesService.findOne('St_Albans'));
  });

  it('/(GET) stores/?page=1', async () => {
    const res: request.Response = await request(app.getHttpServer()).get(
      '/stores/?page=1',
    );
    expect(res.status).toEqual(200);
    expect(res.type).toEqual('application/json');
    expect(res.body.result).toHaveProperty('page');
    expect(res.body.result).toHaveProperty('pageCount');
    expect(res.body.result).toHaveProperty('items');
    expect(res.body.result.items).toBeInstanceOf(Array);
  });

  afterAll(async () => {
    await app.close();
  });
});
